`gwpopulation_pipe` provides a simple way to perform population analyses.

More information can be found in the [online documentation](https://docs.ligo.org/RatesAndPopulations/gwpopulation_pipe).

### Installation

`gwpopulation_pipe` is available via pypi

```console
$ pip install gwpopulation_pipe
```

If you want to use the gpu acceleration the easiest way is to

```console
$ conda install cupy
```